
data_name = "gl-latlong-1km-landcover.bsq"

import numpy as np
import matplotlib.pyplot as plt
from pyparsing import col
raw_data_methods_1 = np.fromfile(data_name)


raw_data_methods_2 = np.memmap(data_name)


data_reshaped = raw_data_methods_2.reshape((21600, 43200))


def latititude_conversion(lat):

    row_num = ((90-lat)/180)*21599

    return int(row_num)


def longtitude_conversion(lat):

    val_lang = 360/43199
    col_num=43199/2 +  lat/val_lang

    return int(col_num)


def water_finder(longtitude, latitude):

    
   col_number = longtitude_conversion(longtitude)

   row_number = latititude_conversion(latitude)
   
   res = data_reshaped[row_number, col_number]

   if res == 0:
        print("It is water")
    
   else:
        print("It is land")


water_finder(0,0)




